(function(FuseBox){FuseBox.$fuse$=FuseBox;
var __process_env__ = {"NODE_ENV":"development"};
FuseBox.pkg("hotlight-docs", {}, function(___scope___){
___scope___.file("docs/index.jsx", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var components_1 = require("./components");
require("../style/Hotlight.scss");
require("./index.scss");
ReactDOM.render(React.createElement(components_1.App, null), document.getElementById("app-root"));
//# sourceMappingURL=index.js.map
});
___scope___.file("docs/components/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./App/App"));
//# sourceMappingURL=index.js.map
});
___scope___.file("docs/components/App/App.jsx", function(exports, require, module, __filename, __dirname){

'use strict';
var __extends = this && this.__extends || function () {
    var extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function (d, b) {
        d.__proto__ = b;
    } || function (d, b) {
        for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p];
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
}();
var __assign = this && this.__assign || Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, '__esModule', { value: true });
var React = require('react');
var components_1 = require('~/components');
var tools_1 = require('~/tools');
var highlightRed = function (frag) {
    return React.createElement('span', {
        style: {
            background: 'red',
            color: 'white'
        }
    }, frag.text);
};
var App = function (_super) {
    __extends(App, _super);
    function App() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            sentences: [
                tools_1.data.splitByRange('Hello world !', [
                    6,
                    11
                ]),
                tools_1.data.splitByText('What are you doing today ?', 'What', 'doing', 'today'),
                [
                    {
                        text: 'I',
                        slot: true
                    },
                    { text: ' would like to ' },
                    {
                        text: 'drink',
                        slot: true
                    },
                    { text: ' some ' },
                    {
                        text: 'water',
                        slot: true
                    },
                    { text: '.' }
                ]
            ],
            selectedData: null
        };
        _this.batchUpdates = null;
        return _this;
    }
    App.prototype.updateSentence = function (newSentence, index) {
        var _this = this;
        this.batchUpdates = (this.batchUpdates ? this.batchUpdates : this.state.sentences).map(function (s, i) {
            return i !== index ? s : newSentence;
        });
        this.setState({ sentences: this.batchUpdates }, function () {
            return _this.batchUpdates = null;
        });
    };
    App.prototype.onResize = function (index) {
        this.setState({ selectedData: null });
    };
    App.prototype.onEdit = function (fragment, index) {
        this.setState({ selectedData: __assign({}, this.state.selectedData, { fragment: fragment }) });
    };
    App.prototype.onSelect = function (fragment, selection, index) {
        this.setState({
            selectedData: {
                fragment: fragment,
                selection: selection,
                index: index
            }
        });
    };
    App.prototype.addSlot = function () {
        var _a = this.state.selectedData, fragment = _a.fragment, selection = _a.selection, index = _a.index;
        this.updateSentence(tools_1.data.addSlot(this.state.sentences[index], fragment, selection), index);
    };
    App.prototype.removeSlot = function () {
        var _a = this.state.selectedData, fragment = _a.fragment, selection = _a.selection, index = _a.index;
        this.updateSentence(tools_1.data.removeSlot(this.state.sentences[index], fragment), index);
    };
    App.prototype.renderSentences = function () {
        var _this = this;
        return this.state.sentences.map(function (sentence, index) {
            return index === 0 ? React.createElement('p', { key: index }, React.createElement(components_1.Hotlight, {
                data: sentence,
                renderSlot: function (_) {
                    return highlightRed(_);
                },
                onResize: function (_) {
                    return _this.onResize(index);
                },
                onEdit: function (_) {
                    return _this.onEdit(_, index);
                },
                onUpdate: function (newSentence) {
                    return _this.updateSentence(newSentence, index);
                }
            })) : index === 1 ? React.createElement('p', { key: index }, React.createElement(components_1.Hotlight, {
                data: sentence,
                onResize: function (_) {
                    return _this.onResize(index);
                },
                onEdit: function (_) {
                    return _this.onEdit(_, index);
                },
                onUpdate: function (newSentence) {
                    return _this.updateSentence(newSentence, index);
                },
                resizable: true
            })) : React.createElement('p', { key: index }, React.createElement(components_1.Hotlight, {
                data: sentence,
                onResize: function (_) {
                    return _this.onResize(index);
                },
                onEdit: function (_) {
                    return _this.onEdit(_, index);
                },
                onUpdate: function (newSentence) {
                    return _this.updateSentence(newSentence, index);
                },
                onSelection: function (frag, selection) {
                    return _this.onSelect(frag, selection, index);
                },
                editable: true,
                resizable: true
            }));
        });
    };
    App.prototype.renderAddSlotBar = function () {
        var _this = this;
        return !this.state.selectedData || this.state.selectedData.fragment.slot || !(this.state.selectedData.selection && this.state.selectedData.selection.toString()) ? null : React.createElement('div', null, this.state.selectedData.selection.toString(), React.createElement('button', {
            onClick: function (_) {
                return _this.addSlot();
            }
        }, 'Add slot'));
    };
    App.prototype.renderRemoveSlotBar = function () {
        var _this = this;
        return !this.state.selectedData || !this.state.selectedData.fragment.slot ? null : React.createElement('div', null, this.state.selectedData.fragment.text, React.createElement('button', {
            onClick: function (_) {
                return _this.removeSlot();
            }
        }, 'Remove slot'));
    };
    App.prototype.render = function () {
        return React.createElement('div', null, this.renderSentences(), this.renderAddSlotBar(), this.renderRemoveSlotBar());
    };
    return App;
}(React.Component);
exports.App = App;
});
___scope___.file("components/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./Hotlight"));
//# sourceMappingURL=index.js.map
});
___scope___.file("components/Hotlight.jsx", function(exports, require, module, __filename, __dirname){

'use strict';
var __extends = this && this.__extends || function () {
    var extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function (d, b) {
        d.__proto__ = b;
    } || function (d, b) {
        for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p];
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
}();
var __assign = this && this.__assign || Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, '__esModule', { value: true });
var React = require('react');
var tools_1 = require('~/tools');
var core_1 = require('~/core');
var Hotlight = function (_super) {
    __extends(Hotlight, _super);
    function Hotlight() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.backgroundColor = tools_1.dom.randomColor();
        _this.color = tools_1.dom.foregroundColor(_this.backgroundColor);
        _this.sentenceNode = null;
        _this.state = { resizeData: null };
        return _this;
    }
    Hotlight.prototype.componentDidMount = function () {
        document.addEventListener('mouseup', this.stopResize.bind(this));
        document.addEventListener('touchend', this.stopResize.bind(this));
        this.props.onUpdate(core_1.core.addIds(this.props.data));
    };
    Hotlight.prototype.componentWillUnmount = function () {
        document.removeEventListener('mouseup', this.stopResize.bind(this));
        document.removeEventListener('touchend', this.stopResize.bind(this));
    };
    Hotlight.prototype.startResize = function (event, index, start) {
        if (start === void 0) {
            start = false;
        }
        var resizeData = {
            node: event.currentTarget.parentNode,
            index: index,
            start: start
        };
        this.setState({ resizeData: resizeData });
    };
    Hotlight.prototype.doResize = function (event) {
        var _this = this;
        if (!this.state.resizeData)
            return;
        var resizeResults = core_1.core.doResize(event, this.props.data, this.state.resizeData);
        if (!resizeResults)
            return;
        var newData = resizeResults.data, newIndex = resizeResults.index;
        this.setState({ resizeData: __assign({}, this.state.resizeData, { index: newIndex }) }, function () {
            if (_this.props.onResize)
                _this.props.onResize(newData[newIndex]);
            _this.props.onUpdate(newData);
        });
    };
    Hotlight.prototype.stopResize = function () {
        this.setState({ resizeData: null });
    };
    Hotlight.prototype.isResizing = function (idx) {
        var resizeData = this.state.resizeData;
        return resizeData && resizeData.index === idx ? resizeData.start ? ' hotlight-expanding hotlight-expanding-start' : ' hotlight-expanding hotlight-expanding-end' : '';
    };
    Hotlight.prototype.doEdit = function (event, index) {
        if (!this.props.editable || !this.sentenceNode)
            return;
        var nodeList = this.sentenceNode.childNodes;
        var newData = core_1.core.generateEditedData(nodeList, this.props.data, index);
        if (this.props.onEdit)
            this.props.onEdit(newData[index]);
        this.props.onUpdate(newData);
    };
    Hotlight.prototype.doSelect = function (fragment) {
        if (!this.props.onSelection)
            return;
        var selection = window.getSelection();
        this.props.onSelection(fragment, selection);
    };
    Hotlight.prototype.renderText = function (fragment, idx) {
        var _this = this;
        var slot = fragment.slot, text = fragment.text, id = fragment.id;
        return React.createElement('span', {
            className: 'hotlight-text',
            key: idx,
            contentEditable: this.props.editable,
            suppressContentEditableWarning: this.props.editable,
            onBlur: function (e) {
                return _this.doEdit(e, idx);
            },
            onSelect: function (e) {
                return _this.doSelect(fragment);
            }
        }, this.props.renderText ? this.props.renderText(fragment) : text);
    };
    Hotlight.prototype.renderExpander = function (idx, start) {
        var _this = this;
        return !this.props.resizable ? null : React.createElement('span', {
            className: 'hotlight-slot-expander hotlight-slot-' + (start ? 'start' : 'end'),
            onMouseDown: function (e) {
                return _this.startResize(e, idx, start);
            },
            onTouchStart: function (e) {
                return _this.startResize(e, idx, start);
            }
        });
    };
    Hotlight.prototype.renderSlot = function (fragment, idx) {
        var _this = this;
        var slot = fragment.slot, text = fragment.text, id = fragment.id;
        if (!this.props.renderSlot) {
            return React.createElement('span', {
                contentEditable: this.props.editable,
                suppressContentEditableWarning: this.props.editable,
                onBlur: function (e) {
                    return _this.doEdit(e, idx);
                },
                onSelect: function (e) {
                    return _this.doSelect(fragment);
                }
            }, text);
        } else {
            return React.cloneElement(this.props.renderSlot(fragment), {
                contentEditable: this.props.editable,
                suppressContentEditableWarning: this.props.editable,
                onBlur: function (e) {
                    return _this.doEdit(e, idx);
                },
                onSelect: function (e) {
                    return _this.doSelect(fragment);
                }
            });
        }
    };
    Hotlight.prototype.renderFragments = function (fragment, idx) {
        var slot = fragment.slot, text = fragment.text, id = fragment.id;
        if (!slot)
            return this.renderText(fragment, idx);
        else if (!id)
            return null;
        else
            return React.createElement('span', {
                className: 'hotlight-slot' + this.isResizing(idx),
                key: id ? id : idx
            }, this.renderExpander(idx, true), this.renderSlot(fragment, idx), this.renderExpander(idx));
    };
    Hotlight.prototype.render = function () {
        var _this = this;
        return React.createElement('span', {
            className: 'hotlight-sentence',
            ref: function (ref) {
                return _this.sentenceNode = ref;
            },
            onKeyPress: function (e) {
                return e.which === 13 ? e.preventDefault() : true;
            },
            onMouseMove: function (e) {
                return _this.doResize(e);
            },
            onTouchMove: function (e) {
                return _this.doResize(e);
            }
        }, this.props.data.map(function (f, i) {
            return _this.renderFragments(f, i);
        }));
    };
    return Hotlight;
}(React.PureComponent);
exports.Hotlight = Hotlight;
});
___scope___.file("tools/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
// Useful tools
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./string"));
__export(require("./dom"));
__export(require("./data"));
//# sourceMappingURL=index.js.map
});
___scope___.file("tools/string.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line:variable-name
exports.string = function (str) { return ({
    /**
     * Truncates a string at max "length" characters (from the right)
     */
    truncateRight: function (length) {
        if (str.length > length) {
            return str.substring(str.length - length);
        }
        return str;
    },
    /**
     * Returns all indexes of an occurrence in a string.
     */
    indexesOf: function (occurrence) {
        var result = [];
        for (var i = 0; i < str.length; i++) {
            if (str.charAt(i) === occurrence.charAt(0) &&
                str.substring(i, i + occurrence.length) === occurrence) {
                result.push(i);
            }
        }
        return result;
    }
}); };
//# sourceMappingURL=string.js.map
});
___scope___.file("tools/dom.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dom = {
    /**
     * Transform a NodeList to an Array
     */
    nodeListToArray: function (nodeList) {
        var array = [];
        // tslint:disable-next-line:prefer-for-of
        for (var i = 0; i < nodeList.length; i++)
            array.push(nodeList[i]);
        return array;
    },
    /**
     * Returns a random color (not too dark nor too bright)
     */
    randomColor: function () {
        var color = ((1 << 24) * Math.random() & 0xF0F0F0 | 0x0F0F0F).toString(16);
        var pad = function (str, length) { return str.length < length ? pad("0" + str, length) : str; };
        return "#" + pad(color, 6);
    },
    /**
     * White or black depending on the background color contrast
     */
    foregroundColor: function (background, threshold) {
        if (threshold === void 0) { threshold = 180; }
        var c = parseInt(background.substring(1), 16);
        if ((c >> 16) + ((c & 0x00FF00) >> 8) + (c & 0x0000FF) >= threshold * 3)
            return "black";
        return "white";
    },
    /**
     * Extract a mouse or touch position.
     */
    extractPosition: function (event) {
        if (event instanceof MouseEvent) {
            return { x: event.clientX, y: event.clientY };
        }
        else if (event instanceof TouchEvent) {
            return { x: event.changedTouches[0].clientX, y: event.changedTouches[0].clientY };
        }
        else {
            return {
                x: event["clientX"] ? event["clientX"] : event["changedTouches"][0].clientX,
                y: event["clientY"] ? event["clientY"] : event["changedTouches"][0].clientY
            };
        }
    },
    /**
     * Cross browser mouse position caret
     */
    getCaret: function (event) {
        var range, offset, node;
        var _a = exports.dom.extractPosition(event), x = _a.x, y = _a.y;
        if (document["caretPositionFromPoint"]) {
            range = document["caretPositionFromPoint"](x, y);
            node = range["offsetNode"];
            offset = range["offset"];
        }
        else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x, y);
            node = range.startContainer;
            offset = range.startOffset;
        }
        return { range: range, offset: offset, node: node };
    }
};
//# sourceMappingURL=dom.js.map
});
___scope___.file("tools/data.js", function(exports, require, module, __filename, __dirname){

'use strict';
var __assign = this && this.__assign || Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, '__esModule', { value: true });
var tools_1 = require('~/tools');
var uuid = require('uuid/v4');
exports.data = {
    splitByRange: function (str) {
        var ranges = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            ranges[_i - 1] = arguments[_i];
        }
        var data = [];
        var index = 0;
        for (var _a = 0, ranges_1 = ranges; _a < ranges_1.length; _a++) {
            var range = ranges_1[_a];
            if (index > range[0])
                throw new Error('Ranges must be sorted in ascending order.');
            data.push({ text: str.substring(index, range[0]) });
            data.push({
                text: str.substring(range[0], range[1]),
                slot: true
            });
            index = range[1];
        }
        if (index < str.length)
            data.push({ text: str.substring(index, str.length) });
        return data;
    },
    splitByText: function (str) {
        var slots = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            slots[_i - 1] = arguments[_i];
        }
        var ranges = [];
        var _loop_1 = function (slot) {
            var index = tools_1.string(str).indexesOf(slot);
            index.forEach(function (idx) {
                return ranges.push([
                    idx,
                    idx + slot.length
                ]);
            });
        };
        for (var _a = 0, slots_1 = slots; _a < slots_1.length; _a++) {
            var slot = slots_1[_a];
            _loop_1(slot);
        }
        return exports.data.splitByRange.apply(exports.data, [str].concat(ranges.sort(function (a, b) {
            return a[0] - b[0];
        })));
    },
    addSlot: function (sentence, fragment, selection, properties) {
        if (properties === void 0) {
            properties = {};
        }
        if (fragment.slot)
            return;
        var newSentence = [];
        var offset = Math.min(selection.anchorOffset, selection.focusOffset);
        for (var _i = 0, sentence_1 = sentence; _i < sentence_1.length; _i++) {
            var frag = sentence_1[_i];
            if (fragment['id'] !== frag['id']) {
                newSentence.push(frag);
                continue;
            }
            var newFragments = [
                {
                    id: uuid(),
                    text: frag.text.substring(0, offset)
                },
                __assign({
                    id: uuid(),
                    text: selection.toString(),
                    slot: true
                }, properties),
                {
                    id: uuid(),
                    text: frag.text.substring(offset + selection.toString().length, frag.text.length)
                }
            ].filter(function (_) {
                return _.text.length > 0;
            });
            newSentence = newSentence.concat(newFragments);
        }
        return newSentence;
    },
    removeSlot: function (sentence, fragment) {
        if (!fragment.slot)
            return;
        var newSentence = [];
        var referenceObj = { text: '' };
        var checkNext = false;
        sentence.forEach(function (frag, index) {
            if (fragment['id'] !== frag['id']) {
                if (checkNext) {
                    checkNext = false;
                    if (!frag.slot) {
                        referenceObj.text = referenceObj.text + frag.text;
                        return;
                    }
                }
                newSentence.push(frag);
                return;
            }
            if (newSentence.length > 0 && !newSentence[newSentence.length - 1].slot) {
                referenceObj = __assign({}, newSentence[newSentence.length - 1]);
                newSentence.splice(index - 1, 1, referenceObj);
            } else {
                newSentence.push(referenceObj);
            }
            referenceObj.text = referenceObj.text + fragment.text;
            checkNext = true;
        });
        return newSentence;
    }
};
});
___scope___.file("core/index.js", function(exports, require, module, __filename, __dirname){

"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./core"));
//# sourceMappingURL=index.js.map
});
___scope___.file("core/core.js", function(exports, require, module, __filename, __dirname){

'use strict';
var __assign = this && this.__assign || Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, '__esModule', { value: true });
var tools_1 = require('~/tools');
var uuid = require('uuid/v4');
exports.core = {
    addIds: function (data) {
        return data.map(function (_) {
            return !_['id'] ? __assign({}, _, { id: uuid() }) : _;
        });
    },
    doResize: function (event, data, expandData) {
        var start = expandData.start, initialIndex = expandData.index;
        var resizeDiffResult = exports.core.resizeDiff(event, data, expandData);
        if (!resizeDiffResult)
            return;
        var newText = resizeDiffResult.newText, widen = resizeDiffResult.widen, diff = resizeDiffResult.diff;
        return exports.core.generateResizedData(data, initialIndex, newText, diff, widen, start);
    },
    resizeDiff: function (event, data, expandData) {
        var _a = tools_1.dom.getCaret(event), cursorRange = _a.range, node = _a.node, offset = _a.offset;
        var start = expandData.start, referenceNode = expandData.node, index = expandData.index;
        var text = data[index].text;
        if (referenceNode.childNodes[1].textContent !== text)
            throw new Error('The rendered text contents and the data object slot text must be equal !\n (' + referenceNode.childNodes[1].textContent + ') !== (' + text + ')');
        var range = document.createRange();
        if (start) {
            range.setEndAfter(referenceNode);
            if (referenceNode.contains(node) || referenceNode.previousSibling && !referenceNode.previousSibling['classList'].contains('hotlight-slot') && referenceNode.previousSibling.contains(node))
                range.setStart(node, offset);
            else
                return;
        } else {
            range.setStartBefore(referenceNode);
            if (referenceNode.contains(node) || referenceNode.nextSibling && !referenceNode.nextSibling['classList'].contains('hotlight-slot') && referenceNode.nextSibling.contains(node))
                range.setEnd(node, offset);
            else
                return;
        }
        var rangeDocument = range.cloneContents();
        var newText = rangeDocument.textContent;
        var big = newText.length > text.length ? newText : text;
        var small = newText.length > text.length ? text : newText;
        var diff = start ? big.substring(0, big.length - small.length) : big.substring(small.length);
        return {
            newText: newText,
            big: big,
            small: small,
            diff: diff,
            widen: big === newText
        };
    },
    generateResizedData: function (currentData, index, newText, diff, widen, start) {
        var newData = currentData.slice();
        if (!widen && start) {
            if (index === 0) {
                newData.unshift({ text: '' });
                index++;
            } else if (currentData[index - 1].slot) {
                newData.splice(index, 0, { text: '' });
                index++;
            }
        } else if (!widen && !start) {
            if (index === currentData.length - 1)
                newData.push({ text: '' });
            else if (currentData[index + 1].slot)
                newData.splice(index + 1, 0, { text: '' });
        }
        var data = newData.map(function (data, idx) {
            if (idx === index) {
                return __assign({}, data, { text: newText });
            }
            if (start && idx === index - 1) {
                var text = widen ? data.text.substring(0, data.text.lastIndexOf(diff)) : data.text + diff;
                return __assign({}, data, { text: text });
            }
            if (!start && idx === index + 1) {
                var text = widen ? data.text.substring(data.text.indexOf(diff.substring(0, data.text.length)) + diff.length) : diff + data.text;
                return __assign({}, data, { text: text });
            }
            return __assign({}, data);
        }).filter(function (_, i) {
            if (_.slot || _.text.length > 0 || i > 0 && i < newData.length - 1)
                return true;
            if (i === 0)
                index--;
            return false;
        });
        return {
            index: index,
            data: data
        };
    },
    generateEditedData: function (nodeList, data, index) {
        return data.map(function (_, i) {
            return i !== index ? _ : __assign({}, _, { text: nodeList.item(i).textContent });
        });
    }
};
});
___scope___.file("style/Hotlight.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("style/Hotlight.scss", ".hotlight-sentence > * {\n  border-top: 0.75em solid transparent; }\n\n.hotlight-sentence [contenteditable=\"true\"] {\n  outline: none;\n  border-bottom: 2px solid transparent;\n  transition: border 0.3s; }\n  .hotlight-sentence [contenteditable=\"true\"]:hover {\n    border-bottom-color: #CCC; }\n  .hotlight-sentence [contenteditable=\"true\"]:focus {\n    border-bottom-color: blue; }\n\n.hotlight-sentence > .hotlight-slot {\n  position: relative; }\n  .hotlight-sentence > .hotlight-slot + .hotlight-slot {\n    margin-left: 2px; }\n  .hotlight-sentence > .hotlight-slot > *:nth-child(2) {\n    display: inline-block;\n    background-color: #ffff88; }\n  .hotlight-sentence > .hotlight-slot .hotlight-slot-expander {\n    display: inline-block;\n    vertical-align: middle;\n    cursor: col-resize;\n    width: 4px;\n    height: 1em;\n    background: #CCC;\n    color: transparent;\n    opacity: 0;\n    z-index: 10;\n    border-bottom: 2px solid transparent;\n    transition: all 0.25s; }\n    .hotlight-sentence > .hotlight-slot .hotlight-slot-expander::before {\n      content: \"\";\n      display: block;\n      position: relative;\n      top: -10px;\n      left: -3px;\n      width: 10px;\n      height: 10px;\n      z-index: 1;\n      background: #CCC;\n      transition: all 0.25s; }\n    .hotlight-sentence > .hotlight-slot .hotlight-slot-expander:hover, .hotlight-sentence > .hotlight-slot .hotlight-slot-expander:hover::before {\n      background: blue; }\n  .hotlight-sentence > .hotlight-slot:hover .hotlight-slot-expander,\n  .hotlight-sentence > .hotlight-slot.hotlight-expanding .hotlight-slot-expander {\n    opacity: 1; }\n  .hotlight-sentence > .hotlight-slot.hotlight-expanding-start .hotlight-slot-start,\n  .hotlight-sentence > .hotlight-slot.hotlight-expanding-end .hotlight-slot-end {\n    background: blue; }\n    .hotlight-sentence > .hotlight-slot.hotlight-expanding-start .hotlight-slot-start::before,\n    .hotlight-sentence > .hotlight-slot.hotlight-expanding-end .hotlight-slot-end::before {\n      background: blue; }\n\n/*# sourceMappingURL=Hotlight.scss.map */")
});
___scope___.file("docs/index.scss", function(exports, require, module, __filename, __dirname){


require("fuse-box-css")("docs/index.scss", "\n/*# sourceMappingURL=index.scss.map */")
});
return ___scope___.entry = "docs/index.tsx";
});
FuseBox.pkg("fusebox-hot-reload", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
/**
 * @module listens to `source-changed` socket events and actions hot reload
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Client = require('fusebox-websocket').SocketClient, bundleErrors = {}, outputElement = document.createElement('div'), styleElement = document.createElement('style'), minimizeToggleId = 'fuse-box-toggle-minimized', hideButtonId = 'fuse-box-hide', expandedOutputClass = 'fuse-box-expanded-output', localStoragePrefix = '__fuse-box_';
function storeSetting(key, value) {
    localStorage[localStoragePrefix + key] = value;
}
function getSetting(key) {
    return localStorage[localStoragePrefix + key] === 'true' ? true : false;
}
var outputInBody = false, outputMinimized = getSetting(minimizeToggleId), outputHidden = false;
outputElement.id = 'fuse-box-output';
styleElement.innerHTML = "\n    #" + outputElement.id + ", #" + outputElement.id + " * {\n        box-sizing: border-box;\n    }\n    #" + outputElement.id + " {\n        z-index: 999999999999;\n        position: fixed;\n        top: 10px;\n        right: 10px;\n        width: 400px;\n        overflow: auto;\n        background: #fdf3f1;\n        border: 1px solid #eca494;\n        border-radius: 5px;\n        font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n        box-shadow: 0px 3px 6px 1px rgba(0,0,0,.15);\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " {\n        height: auto;\n        width: auto;\n        left: 10px;\n        max-height: calc(100vh - 50px);\n    }\n    #" + outputElement.id + " .fuse-box-errors {\n        display: none;\n    }\n    #" + outputElement.id + "." + expandedOutputClass + " .fuse-box-errors {\n        display: block;\n        border-top: 1px solid #eca494;\n        padding: 0 10px;\n    }\n    #" + outputElement.id + " button {\n        border: 1px solid #eca494;\n        padding: 5px 10px;\n        border-radius: 4px;\n        margin-left: 5px;\n        background-color: white;\n        color: black;\n        box-shadow: 0px 2px 2px 0px rgba(0,0,0,.05);\n    }\n    #" + outputElement.id + " .fuse-box-header {\n        padding: 10px;\n    }\n    #" + outputElement.id + " .fuse-box-header h4 {\n        display: inline-block;\n        margin: 4px;\n    }";
styleElement.type = 'text/css';
document.getElementsByTagName('head')[0].appendChild(styleElement);
function displayBundleErrors() {
    var errorMessages = Object.keys(bundleErrors).reduce(function (allMessages, bundleName) {
        var bundleMessages = bundleErrors[bundleName];
        return allMessages.concat(bundleMessages.map(function (message) {
            var messageOutput = message
                .replace(/\n/g, '<br>')
                .replace(/\t/g, '&nbsp;&nbps;&npbs;&nbps;')
                .replace(/ /g, '&nbsp;');
            return "<pre>" + messageOutput + "</pre>";
        }));
    }, []), errorOutput = errorMessages.join('');
    if (errorOutput && !outputHidden) {
        outputElement.innerHTML = "\n        <div class=\"fuse-box-header\" style=\"\">\n            <h4 style=\"\">Fuse Box Bundle Errors (" + errorMessages.length + "):</h4>\n            <div style=\"float: right;\">\n                <button id=\"" + minimizeToggleId + "\">" + (outputMinimized ? 'Expand' : 'Minimize') + "</button>\n                <button id=\"" + hideButtonId + "\">Hide</button>\n            </div>\n        </div>\n        <div class=\"fuse-box-errors\">\n            " + errorOutput + "\n        </div>\n        ";
        document.body.appendChild(outputElement);
        outputElement.className = outputMinimized ? '' : expandedOutputClass;
        outputInBody = true;
        document.getElementById(minimizeToggleId).onclick = function () {
            outputMinimized = !outputMinimized;
            storeSetting(minimizeToggleId, outputMinimized);
            displayBundleErrors();
        };
        document.getElementById(hideButtonId).onclick = function () {
            outputHidden = true;
            displayBundleErrors();
        };
    }
    else if (outputInBody) {
        document.body.removeChild(outputElement);
        outputInBody = false;
    }
}
exports.connect = function (port, uri, reloadFullPage) {
    if (FuseBox.isServer) {
        return;
    }
    port = port || window.location.port;
    var client = new Client({
        port: port,
        uri: uri,
    });
    client.connect();
    client.on('source-changed', function (data) {
        console.info("%cupdate \"" + data.path + "\"", 'color: #237abe');
        if (reloadFullPage) {
            return window.location.reload();
        }
        /**
         * If a plugin handles this request then we don't have to do anything
         **/
        for (var index = 0; index < FuseBox.plugins.length; index++) {
            var plugin = FuseBox.plugins[index];
            if (plugin.hmrUpdate && plugin.hmrUpdate(data)) {
                return;
            }
        }
        if (data.type === "hosted-css") {
            var fileId = data.path.replace(/^\//, '').replace(/[\.\/]+/g, '-');
            var existing = document.getElementById(fileId);
            if (existing) {
                existing.setAttribute("href", data.path + "?" + new Date().getTime());
            }
            else {
                var node = document.createElement('link');
                node.id = fileId;
                node.type = 'text/css';
                node.rel = 'stylesheet';
                node.href = data.path;
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
        if (data.type === 'js' || data.type === "css") {
            FuseBox.flush();
            FuseBox.dynamic(data.path, data.content);
            if (FuseBox.mainFile) {
                try {
                    FuseBox.import(FuseBox.mainFile);
                }
                catch (e) {
                    if (typeof e === 'string') {
                        if (/not found/.test(e)) {
                            return window.location.reload();
                        }
                    }
                    console.error(e);
                }
            }
        }
    });
    client.on('error', function (error) {
        console.log(error);
    });
    client.on('bundle-error', function (_a) {
        var bundleName = _a.bundleName, message = _a.message;
        console.error("Bundle error in " + bundleName + ": " + message);
        var errorsForBundle = bundleErrors[bundleName] || [];
        errorsForBundle.push(message);
        bundleErrors[bundleName] = errorsForBundle;
        displayBundleErrors();
    });
    client.on('update-bundle-errors', function (_a) {
        var bundleName = _a.bundleName, messages = _a.messages;
        messages.forEach(function (message) { return console.error("Bundle error in " + bundleName + ": " + message); });
        bundleErrors[bundleName] = messages;
        displayBundleErrors();
    });
};

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("fusebox-websocket", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var events = require('events');
var SocketClient = /** @class */ (function () {
    function SocketClient(opts) {
        opts = opts || {};
        var port = opts.port || window.location.port;
        var protocol = location.protocol === 'https:' ? 'wss://' : 'ws://';
        var domain = location.hostname || 'localhost';
        this.url = opts.host || "" + protocol + domain + ":" + port;
        if (opts.uri) {
            this.url = opts.uri;
        }
        this.authSent = false;
        this.emitter = new events.EventEmitter();
    }
    SocketClient.prototype.reconnect = function (fn) {
        var _this = this;
        setTimeout(function () {
            _this.emitter.emit('reconnect', { message: 'Trying to reconnect' });
            _this.connect(fn);
        }, 5000);
    };
    SocketClient.prototype.on = function (event, fn) {
        this.emitter.on(event, fn);
    };
    SocketClient.prototype.connect = function (fn) {
        var _this = this;
        console.log('%cConnecting to fusebox HMR at ' + this.url, 'color: #237abe');
        setTimeout(function () {
            _this.client = new WebSocket(_this.url);
            _this.bindEvents(fn);
        }, 0);
    };
    SocketClient.prototype.close = function () {
        this.client.close();
    };
    SocketClient.prototype.send = function (eventName, data) {
        if (this.client.readyState === 1) {
            this.client.send(JSON.stringify({ event: eventName, data: data || {} }));
        }
    };
    SocketClient.prototype.error = function (data) {
        this.emitter.emit('error', data);
    };
    /** Wires up the socket client messages to be emitted on our event emitter */
    SocketClient.prototype.bindEvents = function (fn) {
        var _this = this;
        this.client.onopen = function (event) {
            console.log('%cConnected', 'color: #237abe');
            if (fn) {
                fn(_this);
            }
        };
        this.client.onerror = function (event) {
            _this.error({ reason: event.reason, message: 'Socket error' });
        };
        this.client.onclose = function (event) {
            _this.emitter.emit('close', { message: 'Socket closed' });
            if (event.code !== 1011) {
                _this.reconnect(fn);
            }
        };
        this.client.onmessage = function (event) {
            var data = event.data;
            if (data) {
                var item = JSON.parse(data);
                _this.emitter.emit(item.type, item.data);
                _this.emitter.emit('*', item);
            }
        };
    };
    return SocketClient;
}());
exports.SocketClient = SocketClient;

});
return ___scope___.entry = "index.js";
});
FuseBox.pkg("events", {}, function(___scope___){
___scope___.file("index.js", function(exports, require, module, __filename, __dirname){

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
if (FuseBox.isServer) {
    module.exports = global.require("events");
} else {
    function EventEmitter() {
        this._events = this._events || {};
        this._maxListeners = this._maxListeners || undefined;
    }
    module.exports = EventEmitter;

    // Backwards-compat with node 0.10.x
    EventEmitter.EventEmitter = EventEmitter;

    EventEmitter.prototype._events = undefined;
    EventEmitter.prototype._maxListeners = undefined;

    // By default EventEmitters will print a warning if more than 10 listeners are
    // added to it. This is a useful default which helps finding memory leaks.
    EventEmitter.defaultMaxListeners = 10;

    // Obviously not all Emitters should be limited to 10. This function allows
    // that to be increased. Set to zero for unlimited.
    EventEmitter.prototype.setMaxListeners = function(n) {
        if (!isNumber(n) || n < 0 || isNaN(n))
            throw TypeError("n must be a positive number");
        this._maxListeners = n;
        return this;
    };

    EventEmitter.prototype.emit = function(type) {
        var er, handler, len, args, i, listeners;

        if (!this._events)
            this._events = {};

        // If there is no 'error' event listener then throw.
        if (type === "error") {
            if (!this._events.error ||
                (isObject(this._events.error) && !this._events.error.length)) {
                er = arguments[1];
                if (er instanceof Error) {
                    throw er; // Unhandled 'error' event
                }
                throw TypeError("Uncaught, unspecified \"error\" event.");
            }
        }

        handler = this._events[type];

        if (isUndefined(handler))
            return false;

        if (isFunction(handler)) {
            switch (arguments.length) {
                // fast cases
                case 1:
                    handler.call(this);
                    break;
                case 2:
                    handler.call(this, arguments[1]);
                    break;
                case 3:
                    handler.call(this, arguments[1], arguments[2]);
                    break;
                    // slower
                default:
                    args = Array.prototype.slice.call(arguments, 1);
                    handler.apply(this, args);
            }
        } else if (isObject(handler)) {
            args = Array.prototype.slice.call(arguments, 1);
            listeners = handler.slice();
            len = listeners.length;
            for (i = 0; i < len; i++)
                listeners[i].apply(this, args);
        }

        return true;
    };

    EventEmitter.prototype.addListener = function(type, listener) {
        var m;

        if (!isFunction(listener))
            throw TypeError("listener must be a function");

        if (!this._events)
            this._events = {};

        // To avoid recursion in the case that type === "newListener"! Before
        // adding it to the listeners, first emit "newListener".
        if (this._events.newListener)
            this.emit("newListener", type,
                isFunction(listener.listener) ?
                listener.listener : listener);

        if (!this._events[type])
        // Optimize the case of one listener. Don't need the extra array object.
            this._events[type] = listener;
        else if (isObject(this._events[type]))
        // If we've already got an array, just append.
            this._events[type].push(listener);
        else
        // Adding the second element, need to change to array.
            this._events[type] = [this._events[type], listener];

        // Check for listener leak
        if (isObject(this._events[type]) && !this._events[type].warned) {
            if (!isUndefined(this._maxListeners)) {
                m = this._maxListeners;
            } else {
                m = EventEmitter.defaultMaxListeners;
            }

            if (m && m > 0 && this._events[type].length > m) {
                this._events[type].warned = true;
                console.error("(node) warning: possible EventEmitter memory " +
                    "leak detected. %d listeners added. " +
                    "Use emitter.setMaxListeners() to increase limit.",
                    this._events[type].length);
                if (typeof console.trace === "function") {
                    // not supported in IE 10
                    console.trace();
                }
            }
        }

        return this;
    };

    EventEmitter.prototype.on = EventEmitter.prototype.addListener;

    EventEmitter.prototype.once = function(type, listener) {
        if (!isFunction(listener))
            throw TypeError("listener must be a function");

        var fired = false;

        function g() {
            this.removeListener(type, g);

            if (!fired) {
                fired = true;
                listener.apply(this, arguments);
            }
        }

        g.listener = listener;
        this.on(type, g);

        return this;
    };

    // emits a 'removeListener' event iff the listener was removed
    EventEmitter.prototype.removeListener = function(type, listener) {
        var list, position, length, i;

        if (!isFunction(listener))
            throw TypeError("listener must be a function");

        if (!this._events || !this._events[type])
            return this;

        list = this._events[type];
        length = list.length;
        position = -1;

        if (list === listener ||
            (isFunction(list.listener) && list.listener === listener)) {
            delete this._events[type];
            if (this._events.removeListener)
                this.emit("removeListener", type, listener);

        } else if (isObject(list)) {
            for (i = length; i-- > 0;) {
                if (list[i] === listener ||
                    (list[i].listener && list[i].listener === listener)) {
                    position = i;
                    break;
                }
            }

            if (position < 0)
                return this;

            if (list.length === 1) {
                list.length = 0;
                delete this._events[type];
            } else {
                list.splice(position, 1);
            }

            if (this._events.removeListener)
                this.emit("removeListener", type, listener);
        }

        return this;
    };

    EventEmitter.prototype.removeAllListeners = function(type) {
        var key, listeners;

        if (!this._events)
            return this;

        // not listening for removeListener, no need to emit
        if (!this._events.removeListener) {
            if (arguments.length === 0)
                this._events = {};
            else if (this._events[type])
                delete this._events[type];
            return this;
        }

        // emit removeListener for all listeners on all events
        if (arguments.length === 0) {
            for (key in this._events) {
                if (key === "removeListener") continue;
                this.removeAllListeners(key);
            }
            this.removeAllListeners("removeListener");
            this._events = {};
            return this;
        }

        listeners = this._events[type];

        if (isFunction(listeners)) {
            this.removeListener(type, listeners);
        } else if (listeners) {
            // LIFO order
            while (listeners.length)
                this.removeListener(type, listeners[listeners.length - 1]);
        }
        delete this._events[type];

        return this;
    };

    EventEmitter.prototype.listeners = function(type) {
        var ret;
        if (!this._events || !this._events[type])
            ret = [];
        else if (isFunction(this._events[type]))
            ret = [this._events[type]];
        else
            ret = this._events[type].slice();
        return ret;
    };

    EventEmitter.prototype.listenerCount = function(type) {
        if (this._events) {
            var evlistener = this._events[type];

            if (isFunction(evlistener))
                return 1;
            else if (evlistener)
                return evlistener.length;
        }
        return 0;
    };

    EventEmitter.listenerCount = function(emitter, type) {
        return emitter.listenerCount(type);
    };

    function isFunction(arg) {
        return typeof arg === "function";
    }

    function isNumber(arg) {
        return typeof arg === "number";
    }

    function isObject(arg) {
        return typeof arg === "object" && arg !== null;
    }

    function isUndefined(arg) {
        return arg === void 0;
    }
}

});
return ___scope___.entry = "index.js";
});
FuseBox.import("fusebox-hot-reload").connect(8080, "", true)
FuseBox.target = "browser"

FuseBox.import("hotlight-docs/docs/index.jsx");
FuseBox.main("hotlight-docs/docs/index.jsx");
FuseBox.defaultPackageName = "hotlight-docs";
})
(function(e){function r(e){var r=e.charCodeAt(0),n=e.charCodeAt(1);if((p||58!==n)&&(r>=97&&r<=122||64===r)){if(64===r){var t=e.split("/"),i=t.splice(2,t.length).join("/");return[t[0]+"/"+t[1],i||void 0]}var o=e.indexOf("/");if(o===-1)return[e];var a=e.substring(0,o),u=e.substring(o+1);return[a,u]}}function n(e){return e.substring(0,e.lastIndexOf("/"))||"./"}function t(){for(var e=[],r=0;r<arguments.length;r++)e[r]=arguments[r];for(var n=[],t=0,i=arguments.length;t<i;t++)n=n.concat(arguments[t].split("/"));for(var o=[],t=0,i=n.length;t<i;t++){var a=n[t];a&&"."!==a&&(".."===a?o.pop():o.push(a))}return""===n[0]&&o.unshift(""),o.join("/")||(o.length?"/":".")}function i(e){var r=e.match(/\.(\w{1,})$/);return r&&r[1]?e:e+".js"}function o(e){if(p){var r,n=document,t=n.getElementsByTagName("head")[0];/\.css$/.test(e)?(r=n.createElement("link"),r.rel="stylesheet",r.type="text/css",r.href=e):(r=n.createElement("script"),r.type="text/javascript",r.src=e,r.async=!0),t.insertBefore(r,t.firstChild)}}function a(e,r){for(var n in e)e.hasOwnProperty(n)&&r(n,e[n])}function u(e){return{server:require(e)}}function f(e,n){var o=n.path||"./",a=n.pkg||"default",f=r(e);if(f&&(o="./",a=f[0],n.v&&n.v[a]&&(a=a+"@"+n.v[a]),e=f[1]),e)if(126===e.charCodeAt(0))e=e.slice(2,e.length),o="./";else if(!p&&(47===e.charCodeAt(0)||58===e.charCodeAt(1)))return u(e);var s=g[a];if(!s){if(p&&"electron"!==x.target)throw"Package not found "+a;return u(a+(e?"/"+e:""))}e=e?e:"./"+s.s.entry;var l,c=t(o,e),d=i(c),v=s.f[d];return!v&&d.indexOf("*")>-1&&(l=d),v||l||(d=t(c,"/","index.js"),v=s.f[d],v||"."!==c||(d=s.s&&s.s.entry||"index.js",v=s.f[d]),v||(d=c+".js",v=s.f[d]),v||(v=s.f[c+".jsx"]),v||(d=c+"/index.jsx",v=s.f[d])),{file:v,wildcard:l,pkgName:a,versions:s.v,filePath:c,validPath:d}}function s(e,r,n){if(void 0===n&&(n={}),!p)return r(/\.(js|json)$/.test(e)?v.require(e):"");if(n&&n.ajaxed===e)return console.error(e,"does not provide a module");var i=new XMLHttpRequest;i.onreadystatechange=function(){if(4==i.readyState)if(200==i.status){var n=i.getResponseHeader("Content-Type"),o=i.responseText;/json/.test(n)?o="module.exports = "+o:/javascript/.test(n)||(o="module.exports = "+JSON.stringify(o));var a=t("./",e);x.dynamic(a,o),r(x.import(e,{ajaxed:e}))}else console.error(e,"not found on request"),r(void 0)},i.open("GET",e,!0),i.send()}function l(e,r){var n=h[e];if(n)for(var t in n){var i=n[t].apply(null,r);if(i===!1)return!1}}function c(e,r){if(void 0===r&&(r={}),58===e.charCodeAt(4)||58===e.charCodeAt(5))return o(e);var t=f(e,r);if(t.server)return t.server;var i=t.file;if(t.wildcard){var a=new RegExp(t.wildcard.replace(/\*/g,"@").replace(/[.?*+^$[\]\\(){}|-]/g,"\\$&").replace(/@@/g,".*").replace(/@/g,"[a-z0-9$_-]+"),"i"),u=g[t.pkgName];if(u){var d={};for(var m in u.f)a.test(m)&&(d[m]=c(t.pkgName+"/"+m));return d}}if(!i){var h="function"==typeof r,x=l("async",[e,r]);if(x===!1)return;return s(e,function(e){return h?r(e):null},r)}var _=t.pkgName;if(i.locals&&i.locals.module)return i.locals.module.exports;var y=i.locals={},w=n(t.validPath);y.exports={},y.module={exports:y.exports},y.require=function(e,r){return c(e,{pkg:_,path:w,v:t.versions})},p||!v.require.main?y.require.main={filename:"./",paths:[]}:y.require.main=v.require.main;var j=[y.module.exports,y.require,y.module,t.validPath,w,_];return l("before-import",j),i.fn.apply(0,j),l("after-import",j),y.module.exports}if(e.FuseBox)return e.FuseBox;var d="undefined"!=typeof WorkerGlobalScope,p="undefined"!=typeof window&&window.navigator||d,v=p?d?{}:window:global;p&&(v.global=d?{}:window),e=p&&"undefined"==typeof __fbx__dnm__?e:module.exports;var m=p?d?{}:window.__fsbx__=window.__fsbx__||{}:v.$fsbx=v.$fsbx||{};p||(v.require=require);var g=m.p=m.p||{},h=m.e=m.e||{},x=function(){function r(){}return r.global=function(e,r){return void 0===r?v[e]:void(v[e]=r)},r.import=function(e,r){return c(e,r)},r.on=function(e,r){h[e]=h[e]||[],h[e].push(r)},r.exists=function(e){try{var r=f(e,{});return void 0!==r.file}catch(e){return!1}},r.remove=function(e){var r=f(e,{}),n=g[r.pkgName];n&&n.f[r.validPath]&&delete n.f[r.validPath]},r.main=function(e){return this.mainFile=e,r.import(e,{})},r.expose=function(r){var n=function(n){var t=r[n].alias,i=c(r[n].pkg);"*"===t?a(i,function(r,n){return e[r]=n}):"object"==typeof t?a(t,function(r,n){return e[n]=i[r]}):e[t]=i};for(var t in r)n(t)},r.dynamic=function(r,n,t){this.pkg(t&&t.pkg||"default",{},function(t){t.file(r,function(r,t,i,o,a){var u=new Function("__fbx__dnm__","exports","require","module","__filename","__dirname","__root__",n);u(!0,r,t,i,o,a,e)})})},r.flush=function(e){var r=g.default;for(var n in r.f)e&&!e(n)||delete r.f[n].locals},r.pkg=function(e,r,n){if(g[e])return n(g[e].s);var t=g[e]={};return t.f={},t.v=r,t.s={file:function(e,r){return t.f[e]={fn:r}}},n(t.s)},r.addPlugin=function(e){this.plugins.push(e)},r.packages=g,r.isBrowser=p,r.isServer=!p,r.plugins=[],r}();return p||(v.FuseBox=x),e.FuseBox=x}(this))
//# sourceMappingURL=app.js.map