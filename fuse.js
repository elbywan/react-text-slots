const { FuseBox, EnvPlugin, CSSPlugin, SassPlugin, CopyPlugin, QuantumPlugin } = require("fuse-box")

const fuse = FuseBox.init({
    homeDir: "src",
    output: "dist/bundle/$name.umd.min.js",
    package: "hot-light",
    cache: false,
    sourceMaps: true,
    alias: {
        "self": "~"
    },
    plugins: [
        EnvPlugin({ NODE_ENV: "production" }),
        [ SassPlugin(), CSSPlugin({ outFile: _ => "dist/hotlight.css" }) ],
        QuantumPlugin({
            treeshake: true,
            uglify: true,
            bakeApiIntoBundle: "hotlight"
        })
    ]
})

fuse.bundle("hotlight").target("browser").instructions("![index.umd.ts]")
fuse.run()