import * as React from "react"
import { dom } from "self/tools"
import { Fragment, ResizeData, core } from "self/core"

/* Types */

export type HotlightProps = {
    data: Fragment[],
    onUpdate: (newData: Fragment[]) => void,
    resizable?: boolean,
    editable?: boolean,
    renderSlot?: (frag: Fragment) => React.ReactElement<any>,
    renderText?: (frag: Fragment) => React.ReactNode,
    onResize?: (frag: Fragment) => void,
    onEdit?: (frag: Fragment) => void,
    onSelection?: (frag: Fragment, selection: Selection) => void
}

export type HotlightState = {
    resizeData?: ResizeData
}

export class Hotlight extends React.PureComponent<HotlightProps, HotlightState> {

    private backgroundColor = dom.randomColor()
    private color = dom.foregroundColor(this.backgroundColor)
    private sentenceNode: HTMLElement = null

    state: HotlightState = { resizeData: null }

    /* Lifecycle */

    componentDidMount() {
        document.addEventListener("mouseup", this.stopResize.bind(this))
        document.addEventListener("touchend", this.stopResize.bind(this))
        this.props.onUpdate(core.addIds(this.props.data))
    }

    componentWillUnmount() {
        document.removeEventListener("mouseup", this.stopResize.bind(this))
        document.removeEventListener("touchend", this.stopResize.bind(this))
    }

    /* Events & Logic */

    private startResize(event: React.MouseEvent<HTMLElement> | React.TouchEvent<HTMLElement>, index: number, start = false) {
        const resizeData = {
            node: event.currentTarget.parentNode,
            index,
            start
        }
        this.setState({ resizeData })
    }

    private doResize(event: React.MouseEvent<HTMLElement> | React.TouchEvent<HTMLElement>) {
        if(!this.state.resizeData) return

        const resizeResults = core.doResize(event as any, this.props.data, this.state.resizeData)
        if(!resizeResults) return

        const { data: newData, index: newIndex } = resizeResults

        this.setState({ resizeData: { ...this.state.resizeData, index: newIndex }}, () => {
            if(this.props.onResize) this.props.onResize(newData[newIndex])
            this.props.onUpdate(newData)
        })
    }

    private stopResize() {
        this.setState({ resizeData: null })
    }

    private isResizing(idx: number) {
        const resizeData = this.state.resizeData
        return resizeData && resizeData.index === idx ?
            resizeData.start ? " hotlight-expanding hotlight-expanding-start" :
            " hotlight-expanding hotlight-expanding-end" :
            ""
    }

    private doEdit(event: React.FocusEvent<HTMLElement>, index: number) {
        if(!this.props.editable || !this.sentenceNode) return

        const nodeList = this.sentenceNode.childNodes
        const newData = core.generateEditedData(nodeList, this.props.data, index)
        if(this.props.onEdit) this.props.onEdit(newData[index])
        this.props.onUpdate(newData)
    }

    private doSelect(fragment: Fragment) {
        if(!this.props.onSelection) return
        const selection = window.getSelection()
        this.props.onSelection(fragment, selection)
    }

    /* Rendering */

    private renderText(fragment: Fragment, idx: number) {
        const { slot, text, id } = fragment

        return (
            <span className="hotlight-text" key={ idx }
                    contentEditable={ this.props.editable }
                    suppressContentEditableWarning={ this.props.editable }
                    onBlur={ e => this.doEdit(e, idx) }
                    onSelect={ e => this.doSelect(fragment) }>
                { this.props.renderText ? this.props.renderText(fragment) : text }
            </span>
        )
    }

    private renderExpander(idx: number, start?: boolean) {
        return !this.props.resizable ? null :
            <span className={ "hotlight-slot-expander hotlight-slot-" + (start ? "start" : "end") }
                    onMouseDown={ e => this.startResize(e, idx, start) }
                    onTouchStart={ e => this.startResize(e, idx, start) }></span>
    }

    private renderSlot(fragment: Fragment, idx: number) {
        const { slot, text, id } = fragment
        // const colors = { color: this.color, backgroundColor: this.backgroundColor }

        if(!this.props.renderSlot) {
            return (
                <span   contentEditable={ this.props.editable }
                        suppressContentEditableWarning={ this.props.editable }
                        onBlur={ e => this.doEdit(e, idx) }
                        onSelect={ e => this.doSelect(fragment) }>
                    { text }
                </span>
            )
        } else {
            return React.cloneElement(this.props.renderSlot(fragment), {
                contentEditable: this.props.editable,
                suppressContentEditableWarning: this.props.editable,
                onBlur: e => this.doEdit(e, idx),
                onSelect: e => this.doSelect(fragment)
            })
        }
    }

    private renderFragments(fragment: Fragment, idx: number) {
        const { slot, text, id } = fragment

        if(!slot)
            return this.renderText(fragment, idx)
        else if(!id)
            return null
        else
            return (
                <span className={ "hotlight-slot" + this.isResizing(idx) } key={ id ? id : idx }>
                    { this.renderExpander(idx, true) }
                    { this.renderSlot(fragment, idx) }
                    { this.renderExpander(idx) }
                </span>
            )
    }

    render() {
        return (
            <span   className="hotlight-sentence"
                    ref={ ref => this.sentenceNode = ref }
                    onKeyPress={ e => e.which === 13 ? e.preventDefault() : true }
                    onMouseMove={ e => this.doResize(e) }
                    onTouchMove={ e => this.doResize(e) }>
                { this.props.data.map((f, i) => this.renderFragments(f, i)) }
            </span>
        )
    }
}
