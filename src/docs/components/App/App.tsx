import * as React from "react"
import { Hotlight } from "self/components"
import { Fragment } from "self/core"
import { data } from "self/tools"

const highlightRed = frag => <span style={{ background: "red", color: "white" }}>{ frag.text }</span>

export class App extends React.Component {

    state = {
        sentences: [
            data.splitByRange("Hello world !", [6, 11]),
            data.splitByText("What are you doing today ?", "What", "doing", "today"),
            [ { text: "I", slot: true }, { text: " would like to "}, { text: "drink", slot: true }, { text: " some " }, { text: "water", slot: true }, { text: "."} ]
        ],
        selectedData: null
    }

    private batchUpdates = null
    updateSentence(newSentence, index) {
        this.batchUpdates = (this.batchUpdates ? this.batchUpdates : this.state.sentences).map((s, i) => i !== index ? s : newSentence)
        this.setState({ sentences: this.batchUpdates }, () => this.batchUpdates = null)
    }

    onResize(index: number) {
        this.setState({ selectedData: null })
    }

    onEdit(fragment: Fragment, index: number) {
        this.setState({ selectedData: { ...this.state.selectedData, fragment } })
    }

    onSelect(fragment: Fragment, selection: Selection, index: number) {
        this.setState({ selectedData: { fragment, selection, index }})
    }

    addSlot() {
        const { fragment, selection, index } = this.state.selectedData
        this.updateSentence(data.addSlot(this.state.sentences[index], fragment, selection), index)
    }

    removeSlot() {
        const { fragment, selection, index } = this.state.selectedData
        this.updateSentence(data.removeSlot(this.state.sentences[index], fragment), index)
    }

    renderSentences() {
        return this.state.sentences.map((sentence, index) =>
            index === 0 ?
                <p key={ index }><Hotlight
                    data={ sentence }
                    renderSlot={ _ => highlightRed(_) }
                    onResize={ _ => this.onResize(index) }
                    onEdit={ _ => this.onEdit(_, index) }
                    onUpdate={ newSentence => this.updateSentence(newSentence, index)} />
                </p> :
            index === 1 ?
                <p key={ index }><Hotlight
                    data={ sentence }
                    onResize={ _ => this.onResize(index) }
                    onEdit={ _ => this.onEdit(_, index) }
                    onUpdate={ newSentence => this.updateSentence(newSentence, index)}
                    resizable={ true }/></p> :
                <p key={ index }><Hotlight
                    data={ sentence }
                    onResize={ _ => this.onResize(index) }
                    onEdit={ _ => this.onEdit(_, index) }
                    onUpdate={ newSentence => this.updateSentence(newSentence, index)}
                    onSelection={ (frag, selection) => this.onSelect(frag, selection, index) }
                    editable={ true } resizable={ true }/></p>
        )
    }

    renderAddSlotBar() {
        return !this.state.selectedData || this.state.selectedData.fragment.slot || !(this.state.selectedData.selection && this.state.selectedData.selection.toString()) ? null :
            <div>{ this.state.selectedData.selection.toString() }<button onClick={ _ => this.addSlot() }>Add slot</button></div>
    }

    renderRemoveSlotBar() {
        return !this.state.selectedData || !this.state.selectedData.fragment.slot ? null :
            <div>{ this.state.selectedData.fragment.text }<button onClick={ _ => this.removeSlot() }>Remove slot</button></div>
    }

    render() {
        return (
            <div>
                { this.renderSentences() }
                { this.renderAddSlotBar() }
                { this.renderRemoveSlotBar() }
            </div>
        )
    }
}
