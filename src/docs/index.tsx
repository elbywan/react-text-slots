import * as React from "react"
import * as ReactDOM from "react-dom"
import { App } from "./components"

import "../style/Hotlight.scss"
import "./index.scss"

ReactDOM.render(<App />, document.getElementById("app-root"))
