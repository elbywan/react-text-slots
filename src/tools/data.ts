import { Fragment } from "self/core"
import { string } from "self/tools"
const uuid = require("uuid/v4")

export const data = {
    /**
     * Split a sentence into a data object given slot ranges.
     */
    splitByRange: (str: string, ...ranges: Array<[number, number]>) => {
        const data = []
        let index = 0
        for(const range of ranges) {
            if(index > range[0]) throw new Error("Ranges must be sorted in ascending order.")

            data.push({ text: str.substring(index, range[0]) })
            data.push({ text: str.substring(range[0], range[1]), slot: true })
            index = range[1]
        }
        if(index < str.length)
            data.push({ text: str.substring(index, str.length )})
        return data
    },
    /**
     * Split a sentence into a data object given slot text.
     */
    splitByText: (str: string, ...slots: string[]) => {
        const ranges: Array<[number, number]> = []

        for(const slot of slots) {
            const index = string(str).indexesOf(slot)
            index.forEach(idx => ranges.push([idx, idx + slot.length]))
        }

        return data.splitByRange(str, ...ranges.sort((a, b) => a[0] - b [0]))
    },
    /**
     * Add selected text as a slot to a sentence.
     */
    addSlot: (sentence: Fragment[], fragment: Fragment, selection: Selection, properties: { [key: string]: any } = {}) => {
        if(fragment.slot) return

        let newSentence = []
        const offset = Math.min(selection.anchorOffset, selection.focusOffset)
        for(const frag of sentence) {
            if(fragment["id"] !== frag["id"]) {
                newSentence.push(frag)
                continue
            }
            const newFragments = [
                { id: uuid(), text: frag.text.substring(0, offset) },
                { id: uuid(), text: selection.toString(), slot: true, ...properties },
                { id: uuid(), text: frag.text.substring(offset + selection.toString().length, frag.text.length) },
            ].filter(_ => _.text.length > 0)
            newSentence = [ ...newSentence, ...newFragments ]
        }
        return newSentence
    },
    /**
     * Remove a slot from a sentence.
     */
    removeSlot: (sentence: Fragment[], fragment: Fragment) => {
        if(!fragment.slot) return

        const newSentence = []
        let referenceObj = { text: "" }
        let checkNext = false
        sentence.forEach((frag, index) => {
            if(fragment["id"] !== frag["id"]) {
                if(checkNext) {
                    checkNext = false
                    if(!frag.slot) {
                        referenceObj.text = referenceObj.text + frag.text
                        return
                    }
                }
                newSentence.push(frag)
                return
            }

            if(newSentence.length > 0 && !newSentence[newSentence.length - 1].slot) {
                referenceObj = { ...newSentence[newSentence.length - 1] }
                newSentence.splice(index - 1, 1,  referenceObj)
            } else {
                newSentence.push(referenceObj)
            }

            referenceObj.text = referenceObj.text + fragment.text

            checkNext = true
        })
        return newSentence
    }
}
