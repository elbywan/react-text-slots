export const dom = {
    /**
     * Transform a NodeList to an Array
     */
    nodeListToArray: (nodeList: NodeList): Node[] => {
        const array = []
        // tslint:disable-next-line:prefer-for-of
        for(let i = 0; i < nodeList.length; i++)
            array.push(nodeList[i])
        return array
    },
    /**
     * Returns a random color (not too dark nor too bright)
     */
    randomColor: () => {
        const color = ((1 << 24) * Math.random() & 0xF0F0F0 | 0x0F0F0F).toString(16)
        const pad = (str, length) => str.length < length ? pad("0" + str, length) : str
        return "#" + pad(color, 6)
    },
    /**
     * White or black depending on the background color contrast
     */
    foregroundColor: (background, threshold = 180) => {
        const c = parseInt(background.substring(1), 16)
        if((c >> 16) + ((c & 0x00FF00) >> 8) + (c & 0x0000FF) >= threshold * 3)
            return "black"
        return "white"
    },
    /**
     * Extract a mouse or touch position.
     */
    extractPosition: (event: MouseEvent | TouchEvent) => {
        if(event instanceof MouseEvent) {
            return { x: event.clientX, y : event.clientY}
        } else if(event instanceof TouchEvent) {
            return { x: event.changedTouches[0].clientX, y: event.changedTouches[0].clientY }
        } else {
            return {
                x: event["clientX"] ? event["clientX"] : event["changedTouches"][0].clientX,
                y: event["clientY"] ? event["clientY"] : event["changedTouches"][0].clientY
            }
        }
    },
    /**
     * Cross browser mouse position caret
     */
    getCaret: (event: MouseEvent | TouchEvent) => {
        let range: Range, offset: number, node: Node
        const { x, y } = dom.extractPosition(event)

        if (document["caretPositionFromPoint"]) {
            range = document["caretPositionFromPoint"](x, y)
            node = range["offsetNode"]
            offset = range["offset"]
        } else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x, y)
            node = range.startContainer
            offset = range.startOffset
        }

        return { range, offset, node }
    }
}
