// tslint:disable-next-line:variable-name
export const string = (str: string) => ({

    /**
     * Truncates a string at max "length" characters (from the right)
     */
    truncateRight: (length: number) => {
        if(str.length > length) {
            return str.substring(str.length - length)
        }
        return str
    },

    /**
     * Returns all indexes of an occurrence in a string.
     */
    indexesOf: (occurrence: string) => {
        const result: number[] = []
        for (let i = 0; i < str.length; i++) {
            if(str.charAt(i) === occurrence.charAt(0) &&
                    str.substring(i, i + occurrence.length) === occurrence) {
                result.push(i)
            }
        }
        return result
    }
})
