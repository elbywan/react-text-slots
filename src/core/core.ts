import { dom } from "self/tools"

const uuid = require("uuid/v4")

export type Fragment = {
    text: string,
    slot?: boolean,
    [key: string]: any
}

export type ResizeData = {
    node: Node,
    index: number,
    start: boolean
}

export const core = {
    addIds: (data: Fragment[]) => data.map(_ => (!_["id"]) ? { ..._, id: uuid() } : _),
    doResize: (event: MouseEvent | TouchEvent, data: Fragment[], expandData: ResizeData) => {
        const { start, index: initialIndex } = expandData

        const resizeDiffResult = core.resizeDiff(event, data, expandData)
        if(!resizeDiffResult) return

        const { newText, widen, diff } = resizeDiffResult
        return core.generateResizedData(data, initialIndex, newText, diff, widen, start)
    },
    resizeDiff: (event: MouseEvent | TouchEvent, data: Fragment[], expandData: ResizeData) => {
        const { range: cursorRange, node, offset } = dom.getCaret(event)
        const { start, node: referenceNode, index } = expandData
        const { text } = data[index]

        if(referenceNode.childNodes[1].textContent !== text)
            throw new Error(`The rendered text contents and the data object slot text must be equal !\n (${ referenceNode.childNodes[1].textContent }) !== (${ text })`)

        // Create the range between the starting point and the mouse position
        const range = document.createRange()

        if(start) {
            // If we use the "left" drag pin
            range.setEndAfter(referenceNode)
            // Prevents slots overlapping
            if(referenceNode.contains(node) ||
                    referenceNode.previousSibling &&
                    !referenceNode.previousSibling["classList"].contains("hotlight-slot") &&
                    referenceNode.previousSibling.contains(node))
                range.setStart(node, offset)
            // Overlap - do nothing
            else return
        } else {
            // Right drag pin
            range.setStartBefore(referenceNode)
            // Prevents slots overlapping
            if(referenceNode.contains(node) ||
                    referenceNode.nextSibling &&
                    !referenceNode.nextSibling["classList"].contains("hotlight-slot") &&
                    referenceNode.nextSibling.contains(node))
                range.setEnd(node, offset)
            // Overlap - do nothing
            else return
        }

        // Analyze the range to determine if the new text is bigger
        const rangeDocument = range.cloneContents()
        const newText = rangeDocument.textContent
        const big = newText.length > text.length ? newText : text
        const small = newText.length > text.length ? text : newText
        const diff = start ? big.substring(0, big.length - small.length) : big.substring(small.length)

        return {
            newText,
            big,
            small,
            diff,
            widen: big === newText
        }
    },
    generateResizedData: (currentData: Fragment[], index: number, newText: string, diff: string, widen: boolean, start: boolean) => {

        const newData = [ ...currentData ]

        // Add/Remove text nodes when resizing "border" slots
        if(!widen && start) {
            if(index === 0) {
                newData.unshift({ text: "" })
                index++
            } else if(currentData[index - 1].slot) {
                newData.splice(index, 0, { text: "" })
                index++
            }
        } else if(!widen && !start) {
            if(index === currentData.length - 1)
                newData.push({ text: "" })
            else if(currentData[index + 1].slot)
                newData.splice(index + 1, 0, { text: "" })
        }

        // Generates new text fragments
        const data = newData
            .map((data, idx) => {
                // The modified slot
                if(idx === index) {
                    return {
                        ...data,
                        text: newText
                    }
                }
                // If we used the left pin, this is the fragment on the left of the slot
                if(start && idx === index - 1) {
                    const text = widen ?
                        data.text.substring(0, data.text.lastIndexOf(/*truncDiff*/diff)) :
                        data.text + diff
                    return {
                        ...data,
                        text
                    }
                }
                // If we used the right pin, this is the fragment on the right of the slot
                if(!start && idx === index + 1) {
                    const text = widen ?
                        data.text.substring(data.text.indexOf(diff.substring(0, data.text.length)) + diff.length) :
                        diff + data.text
                    return {
                        ...data,
                        text
                    }
                }
                // Other fragments
                return { ...data }
            }).filter((_, i) => {
                // "Trim" empty fragments at the boundaries of the sentence
                if(_.slot || _.text.length > 0 || (i > 0 && i < newData.length - 1)) return true
                if(i === 0) index--
                return false
            })

        return {
            index,
            data
        }
    },
    // Generate new data from DOM nodes
    generateEditedData: (nodeList: NodeList, data: Fragment[], index) => {
        return data.map((_, i) => i !== index ? _ : {
            ..._,
            text: nodeList.item(i).textContent
        })
    }
}
