const { FuseBox, Sparky, EnvPlugin, SassPlugin,
    CSSPlugin, WebIndexPlugin, QuantumPlugin } = require("fuse-box")

let fuse, vendor, app, production

Sparky.task("config", () => {
    fuse = FuseBox.init({
        homeDir: "src",
        output: "docs/$name.js",
        package: "hotlight-docs",
        hash: production,
        cache: !production,
        sourceMaps: true,
        alias: {
            "self": "~"
        },
        plugins: [
            EnvPlugin({ NODE_ENV: production ? "production" : "development" }),
            [ SassPlugin(), CSSPlugin() ],
            WebIndexPlugin({
                template: "src/docs/index.html",
                target: "index.html"
            }),
            production && QuantumPlugin({
                treeshake: true,
                uglify: true
            })
        ]
    })

    vendor = fuse.bundle("vendor").target("browser").instructions("~ docs/index.tsx")
    app = fuse.bundle("app").target("browser").instructions("> [docs/index.tsx]")
})

Sparky.task("clean", () => Sparky.src("docs/").clean("docs/"))
Sparky.task("prod-env", ["clean"], () => { production = true })
Sparky.task("build", ["prod-env", "config"], () => fuse.run())
Sparky.task("default", ["clean", "config"], () => {
    fuse.dev({
        port : 8080,
        root: "docs"
    })
    app.watch().hmr({ reload: true })
    if(!production) vendor.hmr()
    return fuse.run()
})